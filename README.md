# W4b

installation :
* d'abord s'assurer de bien avoir Node JS d'installé
* démarrer terminal (visual studio code ou git bash) et entrer "npm" 
* si npm est à jour, installer react : "npm install -g create-react-app"
* créer nouveau projet : "npx create-react-app [nom projet]"
* pour pouvoir faire des requêtes à un serveur : "npm install react-router-dom"

utilisation :
se rendre à la racine du projet puis, via le terminal, rentrer : "npm start"
le client va se mettre en marche et ouvrira une page dans le navigateur, en local, sur http://localhost:3000/