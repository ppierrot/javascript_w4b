import React from 'react';
import ListAssiette from '../component/ListAssiette';
import ListMenu from '../component/ListMenu';
import { Route, Link, HashRouter } from "react-router-dom";

function Listdassiette () {
  return <>
    <h1> Liste des assiettes </h1>
    <ListAssiette/>
  </>;
}

function Listdemenu () {
  return <>
    <h1> Liste des menus </h1>
    <ListMenu />
  </>;
}

const Interface = () => (
  <div>
    <h2>Afficher :</h2>
    <Link to="/assiettes"> <p>liste d'assiettes</p> </Link>
    <Link to="/menus"> <p>liste de menus</p> </Link>
  </div>
);

export default class Navigation extends React.Component {

  render() {
    return (
      <HashRouter>
        <Interface/>
        <Route exact path="/assiettes" component={Listdassiette} />
        <Route path="/menus" component={Listdemenu} />
      </HashRouter>
    );
  }
}

