//on importe de quoi vérifier si on a bien reçu une donnée
import { checkStatus } from '../utils/utils';

//port où le serveur fonctionne
const adresse = "http://localhost:3344/"; 

const api = {
    //envoyer une requête pour obtenir tous les objets person
    getAssiettes: () => {
        
        var content = [
            {
                "id": 1,
                "titre": "premier",
                "prix": "20",
                "type": "entree"
            },
            {
                "id": 2,
                "titre": "second",
                "prix": "15",
                "type": "plat"
            }
        ];
        return Promise.resolve(content)/*
        //L'API Fetch fournit une interface JavaScript pour l'accès et la manipulation des parties de la pipeline HTTP, comme les requêtes et les réponses (de base il fiat des requêtes GET)
        return fetch(adresse + "assiette")
        //on regarde si on reçoit bien une donnée avec checkStatus
        .then(checkStatus)
        .then((res) => res.json())*/
    },

    //obtenir tous les groupes
    getMenus: () => {
        
        var content = [
            {
                "id": 1,
                "titre": "menu1",
            },
            {
                "id": 3,
                "titre": "menu2"
            }
        ];
        return Promise.resolve(content)/*
        return fetch(adresse + "menu")
        .then(checkStatus)
        .then((res) => res.json())*/
    },
    
    //pour obtenir tous les membres d'un group avec son id
    getMenuAssiettes: (id) => {
        return fetch(adresse + "menu/" + id + "/assiette")
        .then(checkStatus)
        .then((res) => res.json())
    },

    getMenuReservations: (id) => {
        return fetch(adresse + "menu/" + id + "/reservation")
        .then(checkStatus)
        .then((res) => res.json())
    },
    
    //pour ajouter un nouveau groupe
    addMenu: (titre) => {
        return (
            fetch(adresse + "menu", {
                method: 'POST',
                headers: {
                    'Accept': 'application/json',
                    'Content-Type': 'application/json',
                },
                body: JSON.stringify({
                    title: titre
                })
            })
        )
    },
    
    //supprimer groupe
    deleteMenu: (id) => {
        return (
            fetch(adresse + "menu/" + id, {
                method: 'DELETE'
            })
        )
    }
    

}
export default api;