export const checkStatus = res => {
    //équivalent a si on recevait un statut 200, donc on a bien reçu une donnée depuis le serveur
    if(res.ok)
        return res;
    else
    //sinon on affiche l'érreur du serveur
        return res.text()
        .then(text => { throw text });
}
