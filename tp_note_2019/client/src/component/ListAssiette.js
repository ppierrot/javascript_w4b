import React, { Component } from 'react';
//pour faire des requêtes http au serveur
import { default as api } from '../utils/api.js';

class ListAssiette extends Component{

    state = {
        dataOk: false, //vérifie qu'on a bien reçu des données
        listAssiettes: [ ]
    }

    //on va chercher la liste de contacts au serveur
    componentDidMount(){
        //requête pour obtenir tous les contacts
        api.getAssiettes()
        //sur une promise, on set le state avec la liste de contacts qu'on a obtenu et on confirme l'obtention de données
        .then( (listContacts) => this.setState({ listContacts, dataOk:true}) )
        .catch(err => {
            alert(err);
            this.setState({dataOk: true});
        });
    }

    //JSX pour générer la liste si on a bien reçu les données
    render() {
        if(this.state.dataOk){
            return <div>
                {this.state.listAssiettes.map((i,idx) => (
                    <ListAssiette key={i.id} item assiette={i}/>
                ))}
            </div>
        } else {
            return <span>Waiting for API...</span>;
        }
    }

}

export default ListAssiette;
