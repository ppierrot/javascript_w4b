import React, { Component } from 'react';
//importe l'objet group
import Menu from './Menu.js';
//pour faire des requêtes http au serveur
import api from '../utils/api.js';

class ListMenu extends Component{

    state = {
        dataOk: false, //vérifie qu'on a bien reçu des données
        listMenu: [ ]
    }

    //on va chercher la liste de groupes au serveur
    componentDidMount(){
        //requête pour obtenir tous les groupes
        api.getMenus()
        //sur une promise, on set le state avec la liste de groupes qu'on a obtenu et on confirme l'obtention de données
        .then((data)=> {
            this.setState({
                listMenu: data,
                dataOk: true
            })
        });
    }


    //JSX pour générer la liste si on a bien reçu les données
    render() {
        if(this.state.dataOk){
            return <div>
                {this.state.listMenu.map((i,idx) => (
                    <ul key={i}>
                        {this.state.listMenu.map( (i,idx) => (
                        <li key={i}>{ i.id }</li>))}
                        <MenuConversion key={i.id} item menu={i}/>
                    </ul>
            ))}
            </div>
        } else {
            return <span>Waiting for API...</span>;
        }
    }

}

//conversion en objet Group avec les données reçues depuis le serveur
class MenuConversion extends Component {
    render(){
        console.log(this.props.menu);
        return <Menu menu={this.props.menu} />;
    }
}

export default ListMenu;
