import React, { Component } from 'react';
//import {default as api} from '../utils/api.js';
import { default as api } from '../utils/api.js';


class Menu extends Component {
    state = {
        listAssiette: [ ],
        listReservation: [ ],
        dataOk: false,
    }

    componentDidMount() {

      api.getMenuAssiettes()
        .then((data) => {       
            this.setState({ 
            listAssiette: data,
            dataOk: true });
        })
        .catch((err) => {
            console.log(err.message);
        });

        api.getMenuReservations()
        .then((data) => {       
            this.setState({ 
            listReservation: data,
            dataOk: true });
        })
        .catch((err) => {
            console.log(err.message);
        });

    }

    //render pour générer la liste
    render(){
        if(this.state.dataOk){
            return <Listage menu = {this.props.menu} listReservation = {this.state.listReservation} listAssiette = {this.state.listAssiette}/>;
        }
        else {
            return <p>Données liste de menu absentes</p>;
        }
    }
}

//JSX pour mettre en forme les informations
function Listage(props) {

  return (
    <div>
    <ul>
        <p>Assiettes :</p>
        {props.listAssiette.map( (i,idx) => (
        <li key={i}>{ i.title + " " + i.prix }</li>
        )) }
    </ul>
    <ul>
        <p>Réservations :</p>
        {props.listReservation.map( (i,idx) => (
        <li key={i}>{ i.nom + " " + i.creneau}</li>
        )) }
    </ul>
    </div>
  );
}

export default (Menu);
