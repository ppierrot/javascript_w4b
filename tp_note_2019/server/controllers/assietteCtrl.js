const db = require('../models');

module.exports = {

  //renvoie l'ensemble des personnes sous la forme d'un tableau d'objets en JSON
  get_all: (req, res) => {
    //si l'attribut lastname dans la query string exite
    if(req.query.titre != null){
    //on récupère tous les person dont le lastname est égal a l'attribut lastname dans la query string  
      data = db.Assiette.findAll({where: {titre: req.query.titre}})
    }
    //sinon on cherche tout regardless
		else{
			data = db.Assiette.findAll({
        order: [ 'titre' ]
      }) 
    }
    return data
    .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
      res.json(p); //ce tableau je veux le transformer en json et en faire une requête http
    })
  },

  //ajoute une nouvelle entité `person` en base à partir des données
  create: (req, res) => {
    //on créé un objet constitué des données dans la requête
    return db.Assiette.create({
        titre: req.body.titre,
        type: req.body.type,
        prix: req.boidy.prix
    //promise pour envoyer le résultat
    })
    .then(result =>
        res.send(result))
  },
  
  /*renvoi la personne en fonction de l'id en param*/
  get_by_id: (req, res) => {
    //on renvoi les objets "person" appartenant à l'objet dont l'id correspond à la variable id en url
		return db.Assiette.findById(req.params.id_assiette)
    .then(persons => {
      res.send(persons);
    })
  },

  //modifie la personne d'identifiant id` à partir des données fournies dans la requête
  update_by_id: (req, res) => {
    //on va modifier en bdd les variables suivants avec les données contenu dans le body de la requête
    db.Assiette.update({
      titre: req.body.titre,
      type: req.body.type,
      prix: req.body.prix
    }, {
      returning: true,
      //on modifiera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id_assiette
      }
    })
    //réponses
    .then(
      console.log('Assiette modified : \nID:' + req.params.id_assiette)
    )
    .returning(
      res.send('Assiette modified : \nID:' + req.params.id_assiette)
    )
    .error(
      console.log('The ID ' + req.params.id_assiette + 'does not exists...')
    )
    .catch(
      console.log('The ID ' + req.params.id_assiette + 'does not exists...')
    )
  },

  //supprime la personne d'identifiant id de la base de données
  delete_by_id: (req, res)=>{
    db.Assiette.destroy({
      //on supprimera si l'id en base de données est égal à celui de la variable id de la requête
      where: {
          id: req.params.id_assiette
      }
    })
    //réponse
    .then(
      res.send('The ID ' + req.params.id_assiette + ' has been deleted...')
    )
  },

  //cherche la person d'identifiant id de l'url puis l'envoi via la requête au prochain middleware
  load_by_id: (req, res, next) => {
    //on cherche l'objet en base de données en fonction de l'id de ce denrier passé dans l'url
		return db.Assiette.findById(req.params.id_assiette)
		.then((person) => {
      //si il n'existe pas on envoi érreur
			if (!person) {
				throw { status: 404, message: 'Requested Person not found' };
      }
      //on charge dans la requête l'objet "person" qu'on a obtenu, dans la requête
      req.assiette = assiette;
      //puis appel la fonction middleware suivant dans la pile, on lui envoi les informations
			return next();
		})
		.catch((err) => next(err));
	},

};