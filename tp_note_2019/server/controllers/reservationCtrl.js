const db = require('../models');

module.exports = {

    //renvoie l'ensemble des groupes existants
    get_all: (req, res) => {
            db.Reservation.findAll({order: [ 'nom' ]
        })
        .then((p)=>{ //tout va être enregistré dans le tableau p, renvoyé dans une promise
        res.json(p); //ce tableau je veux le transformer en json et en faire une requête http      
        })
    },

    //créé un groupe avec un champs "tile" dans le corps de la requête
    create: (req, res) => {
        return db.Reservation.create({
            nom: req.body.nom,
            creneau: req.body.creneau,
            MenuId: req.params.id_menu
        //promise pour envoyer le résultat
        })
        .then(result =>
            res.send(result))
    },

    //cherche un group par son identifiant
    get_by_id: (req, res) => {
        return db.Reservation.findById(req.params.reservation_id)
        .then(groups => {
        res.send(groups);
        })
    },

    //modifie le title d'un groupe avec le champs "tile" dans le corps de la requête
    update_by_id: (req, res) => {
        db.Reservation.update({
            nom: req.body.nom,
            creneau: req.body.creneau
          }, {
            returning: true,
            //on modifiera si l'id en base de données est égal à celui de la variable id de l'url
            where: {
                id: req.params.reservation_id
            }
          })
          //réponses
          .then(
            console.log('Reservation modified : \nID:' + req.params.reservation_id)
          )
          .returning(
            res.send('Reservation modified : \nID:' + req.params.reservation_id)
          )
          .error(
            console.log('The ID ' + req.params.reservation_id + 'does not exists...')
          )
          .catch(
            console.log('The ID ' + req.params.reservation_id + 'does not exists...')
          )
    },

    //supprime un groupe
    delete_by_id: (req, res)=>{
        db.Reservation.destroy({
            //on supprimera si l'id en base de données est égal à celui de la variable id de l'url
            where: {
                id: req.params.reservation_id
            }
          })
          //réponse
          .then(
            res.send('The ID ' + req.params.reservation_id + ' has been deleted...')
          )
    },

    //cherche le group d'identifiant id de l'url puis l'envoi via la requête au prochain middleware
    load_reservation: (req, res, next) => {
      //cherche par id dans la bdd
      return db.Reservation.findById(req.params.reservation_id)
      //si on a rien trouvé : error 404 sinon
      .then((g) => {
        if (!g) {
          throw {
            status: 404,
            message: 'Group not found'
          }};
        //on enregistre ce qu'on a trouvé dans la promise
        req.reservation = g;
        next();
      })
      //on renvoi la promise et son contneu vers le prochain middleware a l'aide de next
      .catch((err) => next(err));
    },
  
}