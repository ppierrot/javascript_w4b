const menuCtrl = require('../controllers/menuCtrl');
const assietteCtrl = require('../controllers/assietteCtrl');
const reservationCtrl = require('../controllers/reservationCtrl');

module.exports = [
		
		//cherche tous les group existants
    {
		url: '/menu',
		method: 'get',
		func: menuCtrl.get_all
    },
		
		//créé un nouveau group
    {
		url: '/menu',
		method: 'post',
		func: menuCtrl.create
    },

		//cherche un group via :group_id
    {
		url: '/menu/:id_menu',
		method: 'get',
		func: menuCtrl.get_by_id
    },

		//modifie l'enregistrement d'id :group_id
    {
		url: '/menu/:id_menu',
		method: 'put',
		func: menuCtrl.update_by_id
    },

		//détruit l'enregistrement d'id :group_id
    {
		url: '/menu/:id_menu',
		method: 'delete',
		func: menuCtrl.delete_by_id
		},

		//on chope les réservations d'un menu
		{
			url: "/menu/:id_menu/reservation",
			method: 'get',
			func: [menuCtrl.load_by_id, reservationCtrl.get_by_id]
		},

		//on chope les assiettes d'un menu
		{
			url: "/menu/:id_menu/assiette",
			method: 'get',
			func: [menuCtrl.load_by_id, assietteCtrl.get_by_id]
		}
	
];