const assietteCtrl = require('../controllers/assietteCtrl');

module.exports = [
	
	{
		url: '/assiette',
		method: 'get',
		func: assietteCtrl.get_all
    },
    
    {
		url: '/assiette',
		method: 'post',
		func: assietteCtrl.create
    },
    
    {
		url: '/assiette/:id_assiette',
		method: 'get',
		func: assietteCtrl.get_by_id
    },

    {
		url: '/assiette/:id_assiette',
		method: 'put',
		func: assietteCtrl.update_by_id
    },

    {
		url: '/assiette/:id_assiette',
		method: 'delete',
		func: assietteCtrl.delete_by_id
    }
];