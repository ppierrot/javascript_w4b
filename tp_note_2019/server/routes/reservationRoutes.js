const groupCtrl = require('../controllers/reservationCtrl');

module.exports = [
		
		//cherche tous les group existants
    {
		url: '/reservation',
		method: 'get',
		func: reservationCtrl.get_all
    },
		
		//créé un nouveau group
    {
		url: '/reservation',
		method: 'post',
		func: reservationCtrl.create
    },

		//cherche un group via :group_id
    {
		url: '/reservation/:reservation_id',
		method: 'get',
		func: reservationCtrl.get_by_id
    },

		//modifie l'enregistrement d'id :group_id
    {
		url: '/reservation/:reservation_id',
		method: 'put',
		func: reservationCtrl.update_by_id
    },

		//détruit l'enregistrement d'id :group_id
    {
		url: '/reservation/:reservation_id',
		method: 'delete',
		func: reservationCtrl.delete_by_id
		}
	
];