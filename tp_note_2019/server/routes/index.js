const
fs = require('fs');

module.exports = (app) => {
	
	// read the files of the current directory
	fs.readdirSync(__dirname)
	.filter((filename) => filename !== 'index.js') // avoid this file
	.forEach((filename) => {
		// load routes array and register them
		// un champ `method` indiquant la méthode HTTP, un champ `url` indiquant l'url de la route, un champ `func` qui peut être un middleware ou un tableau de middlewares
		require('./' + filename).forEach((r) => {
			app[r.method](r.url, r.func);
		});
	});

};