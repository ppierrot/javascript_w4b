//on appel body parser pour décrypter les données qu'on reçoit dans une requête
const bodyParser = require('body-parser');
//on déclare la libraire express et on le met dans la variable app 
const express = require('express')
const app = express(); //framework pour gérer le routage des liens URL

//pour automatiquement parser les données et les convertir en json quand on utilise app
app.use(bodyParser.urlencoded({ extended: false }));
app.use(bodyParser.json());

//Gestion d'erreurs (Middleware)
app.use(function (err, req, res, next) {
  console.error(err.stack);
  res.status(500).send('Something broke!');
});

//midleware pour pouvoir faire des requêtes sur ce serveur depuis un client
app.use(function(req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'PUT, POST, DELETE, GET, OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type');
  next();
});

require('./routes/index')(app);

//si on est connécté avec succès sur le port 3000, affiche le log suivant
app.listen(3344, () => console.log('Example app listening on port 3344!'))