//`DataTypes` qui contient l'ensemble des types de données que l'on peut utiliser pour définir les champs des entités
module.exports = function (sequelize, DataTypes) {

	const Menu = sequelize.define(
		'Menu',
		{
			titre:  DataTypes.STRING
		}
	);
    //Cette fonction doit faire partie des méthodes du modèle, elle reçoit l'objet `db` en paramètre et déclare d'éventuelles relations entre ce modèle et d'autres modèles définis dans `db`.
	Menu.associate = (db) => {
		Menu.hasMany(db.Assiette);
		Menu.hasMany(db.Reservation, { onDelete: 'cascade' });
	};

	return Menu;

};