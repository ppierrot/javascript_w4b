//`DataTypes` qui contient l'ensemble des types de données que l'on peut utiliser pour définir les champs des entités
module.exports = function (sequelize, DataTypes) {

	const Reservation = sequelize.define(
		'Reservation',
		{
			nom:  DataTypes.STRING,
			creneau:  DataTypes.ENUM('midi', 'soir'),
		}
	);
    //Cette fonction doit faire partie des méthodes du modèle, elle reçoit l'objet `db` en paramètre et déclare d'éventuelles relations entre ce modèle et d'autres modèles définis dans `db`.
	Reservation.associate = (db) => {
		Reservation.belongsTo(db.Menu);
	};
	
	return Reservation;

};