import React, { Component } from 'react';

class Clock extends Component {
	// valeur initiale du state
	state = {
		time: new Date().toLocaleTimeString()
	};

	// propriété de l'instance servant à stocker l'identifiant de l'intervalle retourné par setInterval
	interval = null;
	
	// exécuté au "montage" de l'instance du composant
	// (le composant est présent lors de ce rendu et ne l'était pas au rendu précédent)
	componentDidMount() {
		this.interval = setInterval(() => {
			// mise à jour du state + demande de re-render du composant
			this.setState({
				time: new Date().toLocaleTimeString()
			});
		}, 1000);
	}

	// exécuté avant que l'instance du composant soit supprimée
	// (le composant était présent au rendu précédent et ne l'est plus lors de ce rendu)
	componentWillUnmount() {
		clearInterval(this.interval);
		// vous pouvez essayer d'enlever cet appel pour voir l'erreur arriver
		// 1 seconde (ou moins) après avoir enlevé la Clock du rendu de l'App
	}
	
	render() {
		return <p>Il est {this.state.time}</p>;
	}
}

class InlineEdit extends Component {
	state = {
		value : this.props.initialValue, //couleur
		editing : false //mode édition activée ou pas
	};

	//changer la la couleur a l'appui du bouton "valider"
	handleChange = (e) => {
		//la propriété onEdited contient la couleur de changement, si le mode édition aussi activée
		if(this.state.editing && this.props.onEdited) {
			//on donne au state value la valeur du props
			this.props.onEdited(this.state.value);
		}
		//on change le statut editing pour lui passer la valeur opposée de ce qu'il est actuellement, donc on désactive le mode édition
		this.setState((prevState) => {return {editing: !prevState.editing};})
	};

	//la state value va prendre la valeur de l'input
	handleTextChange = (e) => {
		this.setState({value: e.target.value});
	};

	render() {
		//si on n'est pas en mode d'édition de la couleur (editing:true), affiche le bouton "modifier"
		if (!this.state.editing) {
			return <div>
				{/*affiche la couleur actuellement appliquée*/}
				<p> { this.state.value } </p>
				{/*bouton "modifier", on appui dessus et on passe en mode d'édition de couleur*/}
				<button onClick={this.handleChange}> Modifier </button>
			</div>
		}
		//sinon, un input contenant par défaut la valeur du state value et un bouton valider
		return <div>
		<input type="text" value={this.state.value} onChange={this.handleTextChange}/>
		<button onClick={this.handleChange}> Valider </button>
	</div>
	};
};

const Box = (props) => {
	let style = {
		borderWidth: '3px',
		borderStyle: 'solid',
		borderColor: 'black',
		backgroundColor: props.backgroundColor || 'pink'
	};
	return <div style={style}>
		{props.children}
	</div>;
};

class App extends Component {
	// valeur initiale du state
	state = {
		showClock: true
	}

	handleChange = (value) => {
		this.setState({color: value});
	}
	
	// cette fonction est passée en tant que prop "onChange" à l'élément "input" rendu;
	// elle sera appelée avec un objet "event" par le gestionnaire d'événements à chaque
	// foid que l'état de l'input aura changé
	showClockChanged = (event) => {
		let value = event.target.checked;
		this.setState({ showClock: value });
	};
	
	render() {
		// la variable "clock" vaut soit un élément "Clock" soit null selon la valeur actuelle de showClock
		let clock = this.state.showClock ? <Clock/> : null;
		return <div>
			<label>Show clock :</label>
			<input type="checkbox" checked={this.state.showClock} onChange={this.showClockChanged}/>
			{/* si "clock" vaut null, les children de l'élément Box seront null, 
			ce qui correspond à ne rien ajouter au DOM résultat */}
			<Box backgroundColor={this.state.borderColor}> { clock } </Box>
			{/* valeur par défaut est test et il y a une propriété onEdited qui changera le state borderColor avec la valeur retournée*/}
			<InlineEdit initialValue="test" onEdited={(v) => this.setState({borderColor:v})}/>
		</div>
	}
}

export default App;
