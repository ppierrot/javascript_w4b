import React, { Component } from 'react';

//component enfant qui générera du html, on passe une propriété du composant parent (app) lors de l'affichage
class Welcome extends Component {
    //constructeur d'objet Welcome
    constructor() {
        super();
        //initialisation d'un objet dont la valeur sera count = 0
        this.state = {
            count: 0
        };
    }

    //méthode pour incrémenter l'attribut count
    //on modifie le state de l'objet Welcome, on va modifier son attribue count pour lui ajouter 1
    //() => permet de directement se binder à this 
    addOne = () => { 
        this.setState({
            count: this.state.count + 1
        });
    }

    //génére du contenu concret, on peut seulement return 1 seul élément, pas plusieurs balises, sinon, utiliser une div
    //on affichera le statut count 
    //le bouton incrémentera le statut count en appellant la méthode au-dessus sur l'appui du bouton
    render(){
        return (
            <div>
                <h1>welcome {this.props.name}</h1>
                <p>mon compteur : {this.state.count}</p>
                <button onClick={this.addOne}>ajouter 1 count</button>
            </div>
        );
    }
}

export default Welcome;