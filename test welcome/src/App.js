import React, { Component } from 'react';
import Welcome from './Welcome';
import './App.css';
//classe parent, on génère avec render le component Welcome, on créé un props "name"
class App extends Component {
  render() {
    return (
     <Welcome name="moi"/>
    );
  }
}

export default App;
