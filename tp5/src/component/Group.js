import React, { Component } from 'react';
//import {default as api} from '../utils/api.js';

class Group extends Component {
    state = {
        listMembre: [ ],
        dataOk: false,
    }

//ne fonctionne pas car n'arrive pas à passer l'id du groupe en paramètre (chercher l'id dans Listgroup)
/*
    componentDidMount() {

      api.getGroupMembers()
        .then((data) => {       
            this.setState({ 
            listMembre: data,
            dataOk: true });
        })
        .catch((err) => {
            console.log(err.message);
        });

    }*/

    //render pour générer la liste
    render(){
        if(this.state.dataOk){
            return <Listage group = {this.props.group} listMembre = {this.state.listMembre}/>;
        }
        else {
            return <p>Données liste de groupe absentes</p>;
        }
    }
}

//JSX pour mettre en forme les informations
function Listage(props) {

  return (
    <ul>
        <p>Membres :</p>
        {props.listMembre.map( (i,idx) => (
        <li key={i}>{ i.firstname + " " + i.lastname }</li>
        )) }
    </ul>
  );
}

export default (Group);
