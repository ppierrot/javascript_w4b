import React, { Component } from 'react';
//importe l'objet group
import Group from './Group.js';
//pour faire des requêtes http au serveur
import api from '../utils/api.js';

class ListGroup extends Component{

    state = {
        dataOk: false, //vérifie qu'on a bien reçu des données
        listGroup: [ ]
    }

    //on va chercher la liste de groupes au serveur
    componentDidMount(){
        //requête pour obtenir tous les groupes
        api.getGroups()
        //sur une promise, on set le state avec la liste de groupes qu'on a obtenu et on confirme l'obtention de données
        .then((data)=> {
            this.setState({
                listGroup: data,
                dataOk: true
            })
        });
    }


    //JSX pour générer la liste si on a bien reçu les données
    render() {
        if(this.state.dataOk){
            return <div>
                {this.state.listGroup.map((i,idx) => (
                    <ul key={i}>
                        {this.state.listGroup.map( (i,idx) => (
                        <li key={i}>{ i.id }</li>))}
                        <GroupConversion key={i.id} item group={i}/>
                    </ul>
            ))}
            </div>
        } else {
            return <span>Waiting for API...</span>;
        }
    }

}

//conversion en objet Group avec les données reçues depuis le serveur
class GroupConversion extends Component {
    render(){
        console.log(this.props.group);
        return <Group group={this.props.group} />;
    }
}

export default ListGroup;
