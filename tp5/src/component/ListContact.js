import React, { Component } from 'react';
//importe l'objet contact
import Contact from './Contact.js';
//pour faire des requêtes http au serveur
import { default as api } from '../utils/api.js';

class ListContact extends Component{

    state = {
        dataOk: false, //vérifie qu'on a bien reçu des données
        listContacts: [ ]
    }

    //on va chercher la liste de contacts au serveur
    componentDidMount(){
        //requête pour obtenir tous les contacts
        api.getPeople()
        //sur une promise, on set le state avec la liste de contacts qu'on a obtenu et on confirme l'obtention de données
        .then( (listContacts) => this.setState({ listContacts, dataOk:true}) )
        .catch(err => {
            alert(err);
            this.setState({dataOk: true});
        });
    }

    //JSX pour générer la liste si on a bien reçu les données
    render() {
        if(this.state.dataOk){
            return <div>
                {this.state.listContacts.map((i,idx) => (
                    <ContactConversion key={i.id} item contact={i}/>
                ))}
            </div>
        } else {
            return <span>Waiting for API...</span>;
        }
    }

}

class ContactConversion extends Component {
    render(){
        console.log(this.props.contact);
        return <Contact contact={this.props.contact}/>;
    }
}

export default ListContact;
