import React, { Component } from 'react';
import {default as api} from '../utils/api.js';

class Contact extends Component {
    state = {
        listPhone: [ ],
        listMail: [],
        listAddress: [ ],
        dataReady: false
    }
    
    componentDidMount() {
    //on va chercher la liste de téléphones via l'id d'un contact sur le serveur, ...
      api.getPhoneFromPerson(this.props.contact.id)
        .then((data) => {
          this.setState({
            listPhone: data,
            dataReady: true
          });
        })
        .catch(err => {
          alert(err);
          this.setState({dataReady: true});
        });

        //... ses adresses mail
        api.getMailFromPerson(this.props.contact.id)
        .then((data)=>{
          this.setState({
            listMail: data,
            dataReady: true
          });
        })
        .catch(err => {
          alert(err);
          this.setState({dataReady: true});
        });

        //... ses adresses postales
        api.getPostalFromPerson( this.props.contact.id)
        .then((data) => {
          this.setState({
            listAddress: data,
            dataReady: true
          });
        })
        .catch(err => {
          alert(err);
          this.setState({dataReady: true});
        }); 

    }

    //render pour générer la liste
    render(){
        if(this.state.dataReady){
            return <Listage contact = {this.props.contact} listPhone = {this.state.listPhone} 
            listAddress = {this.state.listAddress} listMail={this.state.listMail} />;
        }
        else {
          return <p>Données liste de contact absentes</p>;
        }
    }
}

//JSX pour mettre en forme les informations
function Listage(props) {

  return (
    <ul>
    <li>{props.contact.firstname + " " + props.contact.lastname}</li>
      <ul>
        <li>
          <p>Téléphone :</p>
          { props.listPhone.map( (i,idx) => (
              <p key={i}>{ i.type  + " : " + i.address }</p>
          )) }
        </li>
        <li>
          <p> Adresse Mail :</p>
          { props.listMail.map( (i,idx) => (
            <p key={i}>{ i.type  + " : " + i.address }</p>
          )) }
        </li>
        <li>
          <p> Adresse Postal :</p>
          { props.listAddress.map( (i,idx) => (
            <p key={i}>{ i.type  + " : " + i.address}</p>
          )) }
        </li>
      </ul>
    </ul>
  );
}

export default (Contact);
