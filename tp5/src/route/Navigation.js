import React from 'react';
import ListContact from '../component/ListContact.js';
import ListGroup from '../component/ListGroup.js';
import Clock from '../component/Clock.js';
import Grocery from '../component/GroceryList';
import { Route, Link, HashRouter } from "react-router-dom";

function Contacts () {
  return <>
    <h1> Liste des contacts </h1>
    <ListContact/>
  </>;
}

function Groupes () {
  return <>
    <h1> Liste des groupes </h1>
    <ListGroup />
  </>;
}

function Horl () {
  return <>
    <h1> Horloge </h1>
    <Clock/>
  </>;
}

function Course () {
  return <>
    <h1> Liste de courses </h1>
    <Grocery/>
  </>;
}

const Menu = () => (
  <div>
    <h2>Afficher :</h2>
    <Link to="/contacts"> <p>liste de contacts</p> </Link>
    <Link to="/groups"> <p>liste de groupes</p> </Link>
    <Link to="/clock"> <p>l'horloge</p> </Link>
    <Link to="/groceryList"> <p>liste de courses</p> </Link>
  </div>
);

export default class Navigation extends React.Component {

  render() {
    return (
      <HashRouter>
        <Menu/>
        <Route exact path="/contacts" component={Contacts} />
        <Route path="/groups" component={Groupes} />
        <Route path="/clock" component={Horl} />
        <Route path="/groceryList" component={Course} />
      </HashRouter>
    );
  }
}

