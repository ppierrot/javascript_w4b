//on importe de quoi vérifier si on a bien reçu une donnée
import { checkStatus } from '../utils/utils';

//port où le serveur fonctionne
const adresse = "http://localhost:3001/"; //problème -> Access to fetch at from origin has been blocked by CORS policy: No 'Access-Control-Allow-Origin' header is present on the requested resource. If an opaque response serves your needs, set the request's mode to 'no-cors' to fetch the resource with CORS disabled.

const api = {
    //envoyer une requête pour obtenir tous les objets person
    
    getPeople: () => {/*
        var content = [
            {
                "id": 1,
                "firstname": "pierre",
                "lastname": "pastore",
                "createdAt": "2019-03-10T09:30:18.000Z",
                "updatedAt": "2019-03-10T09:30:18.000Z"
            },
            {
                "id": 2,
                "firstname": "paul",
                "lastname": "pastore",
                "createdAt": "2019-03-10T09:30:27.000Z",
                "updatedAt": "2019-03-10T09:30:27.000Z"
            },
            {
                "id": 3,
                "firstname": "chantal",
                "lastname": "pastore",
                "createdAt": "2019-03-10T09:30:32.000Z",
                "updatedAt": "2019-03-10T09:30:32.000Z"
            }
        ];
        return Promise.resolve(content)*/
        //L'API Fetch fournit une interface JavaScript pour l'accès et la manipulation des parties de la pipeline HTTP, comme les requêtes et les réponses (de base il fiat des requêtes GET)
        return fetch(adresse + "person")
        //on regarde si on reçoit bien une donnée avec checkStatus
        .then(checkStatus)
        .then((res) => res.json())
    },

    //pour obtenir les mails d'une person par son id
    getMailFromPerson: (id) => {/*
        var content;
        if (id===1){
            content = [
                {
                    "id": 1,
                    "address": "01@mail.fr",
                    "type": "work",
                    "createdAt": "2019-03-31T13:37:44.000Z",
                    "updatedAt": "2019-03-31T13:37:44.000Z",
                    "PersonId": 1
                }
            ];
        }else if(id===2){
            content = [
                {
                    "id": 1,
                    "address": "02@mail.fr",
                    "type": "home",
                    "createdAt": "2019-03-31T13:37:44.000Z",
                    "updatedAt": "2019-03-31T13:37:44.000Z",
                    "PersonId": 2
                }
            ];
        }else if(id===3){
            content = [
                {
                    "id": 1,
                    "address": "03@mail.fr",
                    "type": "work",
                    "createdAt": "2019-03-31T13:37:44.000Z",
                    "updatedAt": "2019-03-31T13:37:44.000Z",
                    "PersonId": 3
                }
            ];
        }
        return Promise.resolve(content)*/
        return fetch(adresse + "person/" + id + "/mailAddress")
        .then(checkStatus)
        .then((res) => res.json())
    },

    //pour obtenir les numéros de téléphone d'une person par son id
    getPhoneFromPerson: (id) => {/*
        var content;
        if (id===1){
            content = [
                {
                    "id": 1,
                    "address": "0102010203",
                    "type": "work",
                    "createdAt": "2019-03-31T13:01:26.000Z",
                    "updatedAt": "2019-03-31T13:01:26.000Z",
                    "PersonId": 1
                }
            ];
        }else if(id===2){
            content = [
                {
                    "id": 1,
                    "address": "0202010203",
                    "type": "home",
                    "createdAt": "2019-03-31T13:01:26.000Z",
                    "updatedAt": "2019-03-31T13:01:26.000Z",
                    "PersonId": 2
                }
            ];
        }else if(id===3){
            content = [
                {
                    "id": 1,
                    "address": "0302010203",
                    "type": "work",
                    "createdAt": "2019-03-31T13:01:26.000Z",
                    "updatedAt": "2019-03-31T13:01:26.000Z",
                    "PersonId": 3
                }
            ];
        }
        return Promise.resolve(content)*/
        return fetch(adresse + "person/" + id + "/phoneNumber")
        .then(checkStatus)
        .then((res) => res.json())
    },

    //pour obtenir les adresses postales d'une person par son id
    getPostalFromPerson: (id) => {/*
        var content;
        if (id===1){
            content = [
                {
                    "id": 1,
                    "address": "1 rue de maison",
                    "type": "work",
                    "PersonId": "1",
                    "updatedAt": "2019-03-31T13:50:47.749Z",
                    "createdAt": "2019-03-31T13:50:47.749Z"
                }
            ];
        }else if(id===2){
            content = [
                {
                    "id": 1,
                    "address": "2 rue de maison",
                    "type": "work",
                    "PersonId": "2",
                    "updatedAt": "2019-03-31T13:50:47.749Z",
                    "createdAt": "2019-03-31T13:50:47.749Z"
                }
            ];
        }else if(id===3){
            content = [
                {
                    "id": 1,
                    "address": "3 rue de maison",
                    "type": "home",
                    "PersonId": "3",
                    "updatedAt": "2019-03-31T13:50:47.749Z",
                    "createdAt": "2019-03-31T13:50:47.749Z"
                }
            ];
        }
        return Promise.resolve(content)*/
        return fetch(adresse + "person/" + id + "/postAddress")
        .then(checkStatus)
        .then((res) => res.json())
    },

    //obtenir tous les groupes
    getGroups: () => {
        var content;
        content = [
            {
                "id": 1,
                "title": "OS",
                "createdAt": "2019-03-10T09:31:32.000Z",
                "updatedAt": "2019-03-10T09:31:32.000Z"
            },
            {
                "id": 2,
                "title": "AT",
                "createdAt": "2019-03-10T09:31:40.000Z",
                "updatedAt": "2019-03-10T09:31:40.000Z"
            }
        ]
        return Promise.resolve(content)
        //return fetch(adresse + "group")
        //.then(checkStatus)
        //.then((res) => res.json())
    },
    
    //pour obtenir tous les membres d'un group avec son id
    getGroupMembers: (id) => {
        var content;
        if (id===1){
            content = [
            {
                "id": 1,
                "firstname": "pierre",
                "lastname": "pastore",
                "createdAt": "2019-03-10T09:30:18.000Z",
                "updatedAt": "2019-03-10T09:30:18.000Z",
                "GroupPerson": {
                    "createdAt": "2019-03-10T09:32:48.000Z",
                    "updatedAt": "2019-03-10T09:32:48.000Z",
                    "GroupId": 1,
                    "title": "OS",
                    "PersonId": 1
                }
            },
            {
                "id": 2,
                "firstname": "paul",
                "lastname": "pastore",
                "createdAt": "2019-03-10T09:30:27.000Z",
                "updatedAt": "2019-03-10T09:30:27.000Z",
                "GroupPerson": {
                    "createdAt": "2019-03-10T09:39:50.000Z",
                    "updatedAt": "2019-03-10T09:39:50.000Z",
                    "GroupId": 1,
                    "title": "OS",
                    "PersonId": 2
                }
            }
        ]}
        else if(id===2){
            content = [
                {
                    "id": 1,
                    "firstname": "chantal",
                    "lastname": "pastore",
                    "createdAt": "2019-03-10T09:30:18.000Z",
                    "updatedAt": "2019-03-10T09:30:18.000Z",
                    "GroupPerson": {
                        "createdAt": "2019-03-10T09:32:48.000Z",
                        "updatedAt": "2019-03-10T09:32:48.000Z",
                        "GroupId": 2,
                        "title": "AT",
                        "PersonId": 3
                    }
                }
            ]
        }
        return Promise.resolve(content)
        //return fetch(adresse + "group/" + id + "/person")
        //.then(checkStatus)
        //.then((res) => res.json())
    },

}
export default api;