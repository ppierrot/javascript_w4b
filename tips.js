//filtrer éléments entre deux tableaux

//La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui remplissent une condition déterminée par la fonction callback.
groups.filter(g =>
    //La méthode every() permet de tester si tous les éléments d'un tableau vérifient une condition donnée par une fonction en argument.
    p.groups.every(gr => gr.id !== g.id)
//La méthode map() crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant.
).map(

)

all()
some()

//JSX
<select value={} onChange={}>
{this.state.groups.map(g => (
    <option value={g.id}>{g.title}</option>
))}
</select>

/////

//pour faire requêtes vers API sans se répéter

export const getPeople = () => {
    return fetch (...)
    .then(...)
    .then(res => res.json());
}

//dans le code on appèlera ensuite : 

getPeople().then(p => )

//si serveur ne marche pas, on peut faire une promise avec notre tableau avec les données à l'intérieur de cette façon :
//La méthode Promise.resolve(valeur) renvoie un objet Promise qui est résolu avec la valeur donnée. Si cette valeur est une promesse, la promesse est renvoyée, si la valeur possède une méthode then, la promesse renvoyée « suivra » cette méthode et prendra son état ; sinon, la promesse renvoyée sera tenue avec la valeur.
return Promise.resolve([
    {...},
]);

//sur fetch on fait automatiquement get
//si on veut faire un post ou un put avec un body en json
//http content type pour avoir la liste des body
const obj

fetch('...', {
    method: 'POST',
    body: JSON.stringify(obj),
    headers: {'Content-Type' : 'application/json'}
})

//transmettre données d'un component à l'autre
//Tu passes des props
//A l'appel du composant tu mets genre 
<Composant monProps={this.state.monProps} />
//Puis ensuite dans le fichier du composant tu l'appelles avec 
console.log(this.props.monProps)
//Aussi valable pour les fonctions
