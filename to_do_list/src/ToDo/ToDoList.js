import React, { Component } from 'react';

class ToDoList extends Component{

    //constructeur pour créer l'objet ToDoList 
    constructor() {
        super();
        this.state = {
            userInput: "",
            items: []
        }
    }

    //la méthode reçoit un évènement de paramètre event grâce au bind sur le input
    onChange = (event) => { 
        //on rend la state userinput égal à la valeur de event
        this.setState({
            userInput: event.target.value
        });
    }

    addToDo = (event) => {
        event.preventDefault(); //empêche la page de se recharger sur l'appui d'un bouton
        this.setState({
            //...this.state.items copie le tableau d'origine mais en y rajoutant également à sa création le input de l'utilisateur
            items: [...this.state.items, this.state.userInput],
            userInput: "" //vide le champs input après insertion dans items
        //permet d'afficher dans la console les attributs du state de this (controler en faisant inspect sur la page puis dans console)
        }, () => console.log(this.state));
    }

    // permet d'afficher les éléments de la liste
    renderToDos() {
        //map bouclera sur chaque éléments de items
        return this.state.items.map((item) => {
            return(
                <div key={item}>
                    {/*on affiche l'item d'index key, et à côté on affiche un bouton pour le supprimer*/}
                    {item} <button onClick={this.deleteToDo}>supprimer</button>
                </div>
            );
        });
    }

    deleteToDo = (event) => {
        event.preventDefault(); //empêche la page de se recharger sur l'appui d'un bouton
        let target = event.target; // je récupère le bouton cliqué
        let parent = target.parentNode; // je récupère le noeud parent 
        let Item = parent.firstChild.nodeValue; // je descend vers l'enfant pour récupérer l'item correspondant au boutton de suppréssion appuyé
        let indexActuel = this.state.items.indexOf(Item); // Grace à l'item retrouver, je récupère le bon index, 
        const array = this.state.items; //variable array avec les éléments de items
        array.splice(indexActuel, 1); //supprime, dans le tableau array, l'entrée à partir de l'index 'index'
        // on met à jour l'attribut items du state de this avec le tableau array
        this.setState({
            items: array
        })
    }

    //générer objet
    render () {
        return(
            <div>
                <h1>ma todo list</h1>
                <form>
                    <input 
                    /* on appel la méthode onchange sur le input */
                    onChange={this.onChange} 
                    /* valeur de base de l'input est une state*/
                    value={this.state.userInput} 
                    type="text" 
                    />
                    {/* bouton qui, si on clique, va appeler la méthode addtodo sur l'objet actuel*/}
                    <button 
                    onClick={this.addToDo}>ajouter
                    </button>
                </form>
                {/* on affiche les éléments de items ici */}
                <div>
                    {this.renderToDos()}
                </div>
            </div>
        )
    }
}

export default ToDoList;