import React, { Component } from 'react';
import './App.css';
//on importe dodolist dans le component parent
import ToDoList from './ToDo/ToDoList';

class App extends Component {
  render() {
    //on intègre todolist dans les composants à générer
    return (
      <ToDoList />
    );
  }
}

export default App;
