import React, { Component } from 'react';
import InlineEdit from './InlineEdit';

//création de l'objet
var Item = {
	id:0,
	name:"name",
	cost:1,
	quantity:1,

	//setters

	modifyCost: function (item, cost){
		item.cost = cost;
		return item;
	},
	modifyQuantity: function (item, quantity){
		item.quantity = quantity;
		return item;
	},
	modifyName: function(item, name){
		item.name = name;
		return item;
	}
}

//composant d'ajout d'objet par utilisateur
class AddItemForm extends Component {
	state = {
		value: ''
	};

	//on modifie le state value avec la valeur rentré par l'utilisateur
	handleChange = (e) => this.setState({ value: e.target.value });

	//gérer l'ajout de données via l'input
	submit = (e) => {
		e.preventDefault(); //évite le refraichissement de la page sur l'appui du bouton
		var item = Object.create(Item); //création d'un objet item
		item.name=this.state.value; //le nom de l'objet item sera égal à la valeur passé en input par l'utilisateur
		this.props.onAddItem(item); //la propriété onAddItem sera égal à l'objet item
		this.setState({ value: '' }); //on vide l'input de toute valeur à la fin
	};

	//on génère JSX
	render() {
		//formulaire qui, si il y a submit, va faire appel à la fonction submit avec l'instance actuel (this)
		return <form onSubmit={this.submit}>
			{/* input qui prendra la valeur du state value et, si il y a modif, fera appel à la fonction handlechange avec l'instance actuel*/}
			<input value={this.state.value} onChange={this.handleChange} />
			<button>Add</button>
		</form>;
	}
}

//affichage de chaque éléments contenu dans la liste items
const ItemList = (props) => {

	return <div>
		<ul>
		{props.items.map((i, idx) => (
			<ListItem item={i} total={props.total} onUpdateTotal={props.onUpdateTotal} onRemoveItem={props.onRemoveItem} onEditItem={props.onEditItem}></ListItem>
		))}
		</ul>
	</div>;

};

//composant de modification de la quantité et du prix de l'objet et affiche le total que ça représente
class ListItem extends Component {
	//quantité et prix par défaut
	state = {
		cost:1,
		quantity:1,
	}

	//met à jour le total quand le prix change
	onPriceChanged = (e) => {
		//le props cost prend la valeur e afin de modifier l'input
		this.props.item.cost = e.target.value;	
		//change l'état cost et met à jour le total en refaisant le calcul avec le nouveau prix
		this.setState({ cost: e.target.value }, () => {
			this.props.onUpdateTotal(this.props.total + this.props.item.cost*this.props.item.quantity);
		});
		console.log(this.props.item);
	}

	//met à jour le total quand la quantité change
	onQuantityChanged = (e) => {
		//le props cost prend la valeur e afin de modifier l'input
		this.props.item.quantity = e.target.value;
		//change l'état quantity et met à jour le total en refaisant le calcul avec la nouvelle quantité
		this.setState({ quantity: e.target.value }, () => {
			this.props.onUpdateTotal(this.props.total + this.props.item.cost*this.props.item.quantity);
		});
		console.log(this.props.item);	
	};

	//génère le JSX
	render(){
		return <div>
		<div>
		<ul>
		<li key={this.props.item.id}>
			<div>  
				{/* inlineedit pour modifier le titre de l'objet */}
				<InlineEdit initialValue={this.props.item.name} onEdited={(value) => this.props.onEditItem(this.props.item.id, value)}></InlineEdit> <br></br>
				Price <input type="number" value={this.props.item.cost} onChange={this.onPriceChanged}/> <br></br>
				Quantity <input type="number" value={this.props.item.quantity} onChange={this.onQuantityChanged}/> <br></br>
				Total <p> {this.props.item.cost*this.props.item.quantity} </p> <br></br>
				<button onClick={() => this.props.onRemoveItem(this.props.item.id)}>DELETE</button> 
			</div>
		</li>
		</ul>
		</div></div>;
	} 
}


//liste d'objets actuellement dans la liste
export default class ItemListApp extends Component {
	state = {
		nextId:0,
		items: [ ],
		total: 0
	};

	//on ajoute un objet à la liste
	addItem = (item) => {
		item.id=this.state.nextId
		this.setState((prevState) => ({
			//on ajoute l'item en paramètre de la fonction addItem à la liste prevState
			items: [ ...prevState.items, item ],
			//incrémente nextId et total
			nextId: this.state.nextId+1,
			//total: this.state.total+1
		}), () =>{
			console.log(this.state.items);
		});
	};

	//on enlève un objet à la liste
 	removeItem = (index) => {
		let item;

		//on cherche l'objet qu'on veut supprimer avec son id
		this.state.items.forEach((i) => {
			if(i.id === Item.id){
				item = i;
			}
		});
		
		this.setState((prevState) => ({
			total: this.state.total - item.quantity*item.cost,
			//on copie le tableau items pour tous les éléments dont l'id n'est pas égal à celui qu'on veut supprimer
			items: prevState.items.filter((obj) => obj.id !== Item.id)
		}));
	}

	//on change le nom de l'objet
	updateComp = (index, value) => {
		console.log(index);
		console.log(value);
		this.state.items.forEach((item)=>{
			if(item.id === index){
				item = Item.modifyName(item, value);
			}
		});
	}

	//met à jour le total avec une nouvelle valeur en paramètre
	updateTotal = (newVal) => {
		this.setState(() => ({
			total: newVal
		}));
	};

	//on envoi en log un JSON de la liste d'objets 
	componentDidMount() {
		console.log(JSON.parse(window.localStorage.getItem('itemList')));
		const data = JSON.parse(window.localStorage.getItem('itemList'));
		if( data ){
			this.setState(data);
		}
	}

	//on converti les states de la liste d'objets JSON en un string
	componentWillUnmount(){
		window.localStorage.setItem('itemList', JSON.stringify(this.state));
	}

	//on génère JSX
	render () {
		return <>
			{/* on appel le composant d'ajout d'utilisateur avec une proprité onAddItem prenant le résultat de la fonction additem avec l'instance du composent */}
			<AddItemForm onAddItem={this.addItem} />
			{/* on appel le composant de listing d'objets avec une propriété items égal au state items*/}
			<ItemList items={this.state.items} total={this.state.total} onRemoveItem={this.removeItem} onEditItem={this.updateComp} onUpdateTotal={this.updateTotal}/>
		</>;
	}
}
