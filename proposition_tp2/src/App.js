import React, { Component } from 'react';

import ItemListApp from './ItemList';

class App extends Component {

	render() {
		return <>
			<h1>TP React</h1>
			<ItemListApp />
		</>;
	}
}

export default App;
