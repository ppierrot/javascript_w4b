import React, { Component } from 'react';

//composant d'ajout d'objet par utilisateur
class AddItemForm extends Component {
	state = {
		value: ''
	};

	//on modifie le state value avec la valeur rentré par l'utilisateur
	handleChange = (e) => this.setState({ value: e.target.value });

	//gérer l'ajout de données via l'input
	submit = (e) => {
		e.preventDefault(); //évite le refraichissement de la page sur l'appui du bouton
		this.props.onAddItem(this.state.value); //la propriété onAddItem sera égal à la valeur du state value
		this.setState({ value: '' }); //on vide l'input de toute valeur à la fin
	};

	//on génère JSX
	render() {
		//formulaire qui, si il y a submit, va faire appel à la fonction submit avec l'instance actuel (this)
		return <form onSubmit={this.submit}>
			{/* input qui prendra la valeur du state value et, si il y a modif, fera appel à la fonction handlechange avec l'instance actuel*/}
			<input value={this.state.value} onChange={this.handleChange} />
			<button>Add</button>
		</form>;
	}
}

//affichage de la liste contenu dans le state items
const ItemList = (props) => {
	return <ul>
		{/*La méthode map() crée un nouveau tableau avec les résultats de l'appel d'une fonction fournie sur chaque élément du tableau appelant. */}
		{props.items.map((i, idx) => (
			/*chaque li pendra la valeur de l'index d'un élément du tableau, on affcihera ensuite l'itération i dedans, puis en cliquant sur le bouton on éfface l'objet d'index idx*/
			<li key={idx}>
				{i}
				<button onClick={() => props.onRemoveItem(idx)}>Remove</button>
			</li>
		))}
	</ul>;
};

//composant de listage d'objets
export default class ItemListApp extends Component {
	state = {
		items: [ 'salut', 'youpi' ]
	};

	////reçoit une chaîne et modifie le state de sorte que le tableau `items` contienne cette chaîne dans une nouvelle case
	addItem = (item) => {
		this.setState((prevState) => ({
			//on ajoute au tableau, contenu dans le state items, l'objet item
			items: [ ...prevState.items, item ]
		}));
	};

	//reçoit un nombre et modifie le state de sorte que le tableau `items` ne contienne plus la case dont l'index correspond au nombre reçu
	removeItem = (index) => {
		//La méthode filter() crée et retourne un nouveau tableau contenant tous les éléments du tableau d'origine qui ne sont pas égal à index (qui est l'élément qu'on veut supprimer), i représente l'itératio, idx représente l'index de l'objet
		this.setState((prevState) => ({
			items: prevState.items.filter((i, idx) => idx !== index)
		}));
	}

	//on génère JSX
	render () {
		return <>
			{/* on appel le composant d'ajout d'utilisateur avec une proprité onAddItem prenant le résultat de la fonction additem avec l'instance du composent */}
			<AddItemForm onAddItem={this.addItem} />
			{/* on appel le composant de listing d'objets avec une propriété items égal au state items*/}
			<ItemList items={this.state.items} onRemoveItem={this.removeItem} />
		</>;
	}
}
